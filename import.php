<?php
require  'vendor/autoload.php';
require_once('export.php');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

if (!function_exists('import_file')) {
	function import_file() {

		$reader = IOFactory::createReader('Xlsx');
		$reader->setReadDataOnly(true);
		$spreadsheet = $reader->load('input.xlsx');

		// Get data in Name Sheet
		$nameSheet = $spreadsheet->getSheet(0);
		$nameData = $nameSheet->rangeToArray(
		    'A2:' . $nameSheet->getHighestColumn() . $nameSheet->getHighestRow(),     // The worksheet range that we want to retrieve
		    NULL,        // Value that should be returned for empty cells
		    TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
		    TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
		    TRUE         // Should the array be indexed by cell row and cell column
		);

		// Get data in Phone Sheet
		$phoneSheet = $spreadsheet->getSheet(1);
		$phoneData = $phoneSheet->rangeToArray(
	        'A1:' . $phoneSheet->getHighestColumn() . $phoneSheet->getHighestRow(),     // The worksheet range that we want to retrieve
	        NULL,        // Value that should be returned for empty cells
	        TRUE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
	        TRUE,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
	        TRUE         // Should the array be indexed by cell row and cell column
	    );

		// Connect DB Obj
		$db = new PDO('sqlite:db.sqlite');
		$query = "DROP TABLE IF EXISTS Name; DROP TABLE IF EXISTS Phone; CREATE TABLE IF NOT EXISTS Name(uid VARCHAR NOT NULL, name VARCHAR NULL, email VARCHAR NULL, PRIMARY KEY (uid)); CREATE TABLE IF NOT EXISTS Phone(uid VARCHAR NOT NULL, phone VARCHAR NULL, PRIMARY KEY (uid))";
		$createTable = $db->exec($query);

		// Insert Name Data to DB
		$insertName = "INSERT INTO Name(uid, name, email) VALUES ";
		foreach ($nameData as $row) {
			$uid = number_format($row['C'], 0,'', '');
			$insertName .= "('{$uid}', '{$row['B']}', '{$row['E']}'),";
		}
		$insertName = substr($insertName, 0, -1);
		$insertTable = $db->exec($insertName);

		//Insert Phone Data to DB
		$insertPhone = "INSERT INTO Phone(uid, phone) VALUES ";
		foreach ($phoneData as $row) {
			$result = explode(' | ', $row['A']);
			$insertPhone .= "('{$result[0]}', '{$result[1]}'),";
		}
		$insertPhone = substr($insertPhone, 0, -1);
		$insertTable = $db->exec($insertPhone);
	
		rename('input.xlsx', time() . '_data.xlsx');
		export_file();
	}
}


